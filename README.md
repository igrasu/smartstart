#SmartStart - Responsive HTML5 Template

SmartStart is a simple and clean but still professional template suitable for any business or portfolio, and it’s created by using the latest HTML5 and CSS3 techniques. With a responsive design it is easily usable with any device (Desktop, tablet, mobile phone…), without removing any content!
![alt text](https://s3.envato.com/files/28124644/SmartStartScreenshots/01_Preview.jpg)
